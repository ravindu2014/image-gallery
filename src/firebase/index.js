import { initializeApp } from 'firebase/app';

import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { getDatabase, ref as dbRef } from "firebase/database";
import { getFirestore, getDocs, addDoc, collection } from "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyCY9mq20gNi87llyOebGfvsbFYFihGIujw",
    authDomain: "image-gallery-techlabs.firebaseapp.com",
    projectId: "image-gallery-techlabs",
    storageBucket: "image-gallery-techlabs.appspot.com",
    messagingSenderId: "832891540987",
    appId: "1:832891540987:web:4654ea898866ee1773275e"
};

const app = initializeApp(firebaseConfig);

const storage = getStorage(app);
const database = getDatabase(app);
const fireStore = getFirestore(app);

export { 
    storage, 
    storageRef, 
    uploadBytesResumable, 
    getDownloadURL, 
    database, 
    dbRef, 
    fireStore, 
    addDoc, 
    getDocs, 
    collection 
};
