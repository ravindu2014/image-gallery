import { useState, useEffect } from 'react';
import { Image, Empty } from 'antd';

import { fireStore, collection, getDocs } from '../../../firebase';
import { Pagination } from '../../common';
import './styles.scss';
import { GALLERY_FALLBACK_IMAGE_STRING } from '../../../constants';

const PAGE_INDEX_INITIALVAL = 1;
const DEFAULT_PAGE_SIZE = 5;

const ImageDisplayPage = () => {
    const [allImages, setAllImages] = useState([]);
    const [currentPage, setCurrentPage] = useState(PAGE_INDEX_INITIALVAL);
    const [pageSize, setPageSize] = useState(DEFAULT_PAGE_SIZE);
    const [totalSize, setTotalSize] = useState(0);

    useEffect(() => {
        const fetchData = async (
            page,
            size
        ) => {
            const querySnapshot = await getDocs(collection(fireStore, "images"));
            const allImageDataArr = [];

            if (querySnapshot.docs) {
                querySnapshot.forEach((doc) => {
                    // doc.data() is never undefined for query doc snapshots                
                    const currentObj = {
                        id: doc.id,
                        title: doc.data().title,
                        description: doc.data().description,
                        imageURL: doc.data().imageURL
                    };
                    allImageDataArr.push(currentObj);                
                });
                const resultSetSize = allImageDataArr.length; // Get the total number of images returned by the firestore
                
                const startingImageIndex = (PAGE_INDEX_INITIALVAL + ((page - 1) * size)) - 1; // Determine first image's index for current page
                const incementedIndex = (page * size); // Determine the maximum index of image that can be displayed on current page
                const endingImageIndex = resultSetSize > incementedIndex ? incementedIndex : resultSetSize; // Determine the maximum index of image that can be actually displayed on current page
                
                const slicedImageDataArray = allImageDataArr.slice(startingImageIndex, endingImageIndex); // Get a slice of images array that really fits to the current page
                
                setTotalSize(resultSetSize);
                setAllImages(slicedImageDataArray);
            }  
        };
        fetchData(currentPage, pageSize);
    }, [currentPage, pageSize, totalSize]);

    return (
        <div className="GalleryWrapper">
            {(allImages && allImages.length > 0) ? (
                <>
                    <div className="ImageDisplaySection">
                        {allImages.map((val, i) => {
                            return (
                                //Image container
                                <Image
                                    key={i}
                                    width={200}
                                    height={180}
                                    src={val.imageURL}
                                    fallback={GALLERY_FALLBACK_IMAGE_STRING}
                                />
                            );
                        })}
                    </div>
                    <Pagination
                        totalCount={totalSize || allImages.length}
                        currentPage={currentPage}
                        showSizeChanger
                        onChange={(page, pageSize) => {
                            setCurrentPage(page);
                            setPageSize(pageSize);
                        }}
                        pageSize={pageSize}
                    />
                </>
            ) : (
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
            )}
        </div>
    );
}

export default ImageDisplayPage;
