import { useState } from 'react';
import { Form, Input, Button, Upload, message } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

import {
    ERROR_MSG_ONLY_IMAGE_FILES,
    ERROR_MSG_SIZE_LIMIT_5MB,
    SUCCUSS_MSG_IMAGE_UPLOAD,
    BUTTON_NAME_UPLOAD,
    FORM_LABEL_TITLE,
    ERROR_MSG_TITLE_FOR_IMAGE,
    FORM_LABEL_DESC,
    ERROR_MSG_DESC_FOR_IMAGE,
    BUTTON_NAME_SUBMIT
 } from '../../../constants';

import { 
    storage, 
    storageRef, 
    uploadBytesResumable,
    getDownloadURL, 
    fireStore, 
    addDoc, 
    collection 
} from '../../../firebase';

const ImageUploadPage = () => {
    const [image, setImage] = useState(null);
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();

    const handleChange = e => { // Handle upload image input changes
        if (e.file.originFileObj) {
            setImage(e.file.originFileObj);
        }
    }

    const beforeUpload = (file) => {
        const isImage = file.type.indexOf('image/') === 0;

        if (!isImage) { // File type validation
            message.error(ERROR_MSG_ONLY_IMAGE_FILES);
        }
        
        // Image size validation
        const isLt5M = file.size / 1024 / 1024 < 5;
        if (!isLt5M) {
            message.error(ERROR_MSG_SIZE_LIMIT_5MB);
        }
        return isImage && isLt5M;
    };

    const handleUpload = (values) => { // Handle upload image button click
        setLoading(true);
        const imageRef = storageRef(storage, `images/${image.name}`);
        const uploadTask = uploadBytesResumable(imageRef, image);

        uploadTask.on('state_changed', 
            (snapshot) => {}, 
            (error) => {}, 
            () => {
                getDownloadURL(uploadTask.snapshot.ref).then((url) => {
                    setLoading(false);
                    handleDataSubmitToFBDB(url, values);
                });
            }
        );
    }

    const handleDataSubmitToFBDB = async (imgUrl, values) => {

        // Persist data to Firebase Firestore Database
        try {
            const fsRef = collection(fireStore, "images");
            await addDoc(fsRef, {
                title: values.title,
                description: values.description,
                imageURL: imgUrl
            });           
        } catch(error) {
            console.error(error);
        }

        message.success(SUCCUSS_MSG_IMAGE_UPLOAD);
        form.resetFields();
        setImage(null);
    }

    const uploadButton = ( // Component to show Upload icon and text inside the upload widget
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div className="ant-upload-text">{BUTTON_NAME_UPLOAD}</div>
        </div>
    );

    return (
        <div className="App">
            <Form
                name="ImageUpload"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={handleUpload}
                autoComplete="off"
                form={form}
                >
                <Form.Item
                    label="Image"
                >
                    <Upload
                        name="avatar"
                        listType="picture-card"
                        className="avatar-uploader"
                        showUploadList={false}
                        onChange={handleChange}
                        beforeUpload={beforeUpload}
                    >
                        {image ? <label style={{ width: '100%' }}>{image.name}</label> : uploadButton}
                    </Upload>
                </Form.Item>
                <Form.Item
                    label={FORM_LABEL_TITLE}
                    name="title"
                    rules={[
                    {
                        required: true,
                        message: ERROR_MSG_TITLE_FOR_IMAGE,
                    },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label={FORM_LABEL_DESC}
                    name="description"
                    rules={[
                    {
                        required: true,
                        message: ERROR_MSG_DESC_FOR_IMAGE,
                    },
                    ]}
                >
                    <Input.TextArea />
                </Form.Item>
                <Form.Item
                    wrapperCol={{
                    offset: 8,
                    span: 16,
                    }}
                >
                    <Button type="primary" htmlType="submit">
                        {BUTTON_NAME_SUBMIT}
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
}

export default ImageUploadPage;
