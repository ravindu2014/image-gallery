import { Link } from 'react-router-dom';

import './Navbar.scss';
import {
    LINK_NAME_IMAGE_UPLOAD,
    LINK_NAME_IMAGE_GALLERY,
    LINK_NAME_IMAGE_SEARCH,
    ROUTE_IMAGE_UPLOAD_PAGE,
    ROUTE_IMAGE_GALLERY_PAGE,
    ROUTE_IMAGE_SEARCH_PAGE
} from '../../../constants';

const Navbar = () => {
    return (
        <div className="NavbarWrapper">
            <div className="HeaderWrapper">
                <div className="HeaderContent">         
                    <Link className="MenuItem" to={ROUTE_IMAGE_UPLOAD_PAGE}>{LINK_NAME_IMAGE_UPLOAD}</Link>
                    <Link className="MenuItem" to={ROUTE_IMAGE_GALLERY_PAGE}>{LINK_NAME_IMAGE_GALLERY}</Link>
                    <Link className="MenuItem" to={ROUTE_IMAGE_SEARCH_PAGE}>{LINK_NAME_IMAGE_SEARCH}</Link>
                </div>
            </div>
        </div>
    );
}

export default Navbar;
