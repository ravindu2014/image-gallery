import React from 'react';
import { Pagination as AntdPagination } from 'antd';

import './styles.scss';

const DEFAULT_PAGE_SIZE = 5;

const Pagination = ({
    totalCount,
    currentPage,
    disabled = false,
    hideOnSinglePage = false,
    pageSize,
    onChange = () => {},
    onShowSizeChange = () => {},
    showSizeChanger = false,
}) => {
    return (
        <div className="PaginationWrapper">
            <AntdPagination
                responsive
                showSizeChanger={showSizeChanger}
                onShowSizeChange={number => {
                    onShowSizeChange(number);
                }}
                current={currentPage}
                hideOnSinglePage={hideOnSinglePage}
                pageSize={pageSize || DEFAULT_PAGE_SIZE}
                total={totalCount}
                onChange={(page, pageSize) => {
                    onChange(page, pageSize);
                }}
                disabled={disabled}
            />
        </div>
    );
};

export default Pagination;
