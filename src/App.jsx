import 'antd/dist/antd.css';
import {
    BrowserRouter as Router,
    Route,
    Switch
} from "react-router-dom";

import './App.scss';
import { Navbar } from './components/common';
import { ImageUploadPage, ImageDisplayPage } from './components/pages';

const App = () => {
    return (
        <Router>
            <div className="App">
                <Navbar />
                <Switch>
                    <Route exact path="/">
                        <ImageUploadPage />
                    </Route>
                    <Route path="/imageupload">
                        <ImageUploadPage />
                    </Route>
                    <Route path="/imagegallery">
                        <ImageDisplayPage />
                    </Route>
                    <Route path="*">
                        <NoMatch />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

const NoMatch = () => {
    return (<p>There's nothing here: 404!</p>);
};

export default App;
