export const ERROR_MSG_ONLY_IMAGE_FILES = 'You can only upload image file!';
export const ERROR_MSG_SIZE_LIMIT_5MB = 'Image must smaller than 5MB!';
export const ERROR_MSG_TITLE_FOR_IMAGE = 'Please give a Title for image!';
export const ERROR_MSG_DESC_FOR_IMAGE = 'Please give a Description for image!';

export const SUCCUSS_MSG_IMAGE_UPLOAD = 'Successfully uploaded the image!';