export const ROUTE_IMAGE_UPLOAD_PAGE = '/imageupload';
export const ROUTE_IMAGE_GALLERY_PAGE = '/imagegallery';
export const ROUTE_IMAGE_SEARCH_PAGE = '/imagesearch';