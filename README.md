Technologies
------------
reactJS
firebase (firebase storage and firestore database)
sass
react routing

Pre-requisites
--------------
You need to have NodeJS and npm installed on your local environment.(Latest versions if you prefer)

Steps to set up the environment
-------------------------------
1. first clone the application form git using the shared repository url
2. Then navigate to the root directory of the application using a cli tool and run command 'npm install'.
3. run the application with the command 'npm start' or yarn if you have it installed.
4. The application will run on your local environment
